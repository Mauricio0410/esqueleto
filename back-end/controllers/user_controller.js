//var express = require('express');
//var userFile = require('./user.json'); //creando referencia a ese archivo
//var bodyParser = require('body-parser');
var requestJson = require('request-json');
//var app = express();
app.use(bodyParser.json());
//var totalUsers = 0; //contador de usuarios
//const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000; //si hay una variable de entorno
const URL_MLAB = "https://api.mlab.com/api/1/databases/techu4db/collections/";
var miAPIKey= "apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";

//GET users consumiendo API REST de mLab
//app.get(URL_BASE + 'mlabusers',
function getUsers(req,res){
    console.log("GET /colapi/v3/users");
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log("Cliente HTTP mLab creado.");
    var qString = 'f={"_id":0}&';
    httpClient.get('user?' + qString + miAPIKey,
      function(err, respuestaMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo usuario"}
          res.status(500);
        }else {
          if (body.length > 0) {
            response = body;
          }else {
            response = {"msg" : "Ningún elemento 'user'"}
            res.status(404);
          }
        }
        res.send(response);
      }
    )
}

module.exports.getUsers = getUsers;
