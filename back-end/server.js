//console.log("Hola Perú");

//Creamos variable que apunte al objeto expreess
// y de ahì podemos usar sus funciones
var express = require('express');
var userFile = require('./user.json'); //creando referencia a ese archivo
var bodyParser = require('body-parser');
var requestJson = require('request-json');
//objetto app que permita a traves de express
//hacer peticiones
//objeto app de express permitira hacer las
//peticiones hhttp de express
var app = express();
app.use(bodyParser.json());
var totalUsers = 0; //contador de usuarios
const URL_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000; //si hay una variable de entorno
const URL_MLAB = "https://api.mlab.com/api/1/databases/techu4db/collections/";
var miAPIKey= "apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";
//const user_controller = require('./controllers/user_controller');


//Peticiòn GET de todos los 'users' (Collections)
app.get(URL_BASE + 'users',
      function(request,response) {
        console.log('GET' + URL_BASE + 'users');
        response.send(userFile);
      });

//Peticiòn GET de un 'users'(Instancia)
app.get(URL_BASE + 'users/:id',//en vez de "id" se puede usar cualquier variable
      function(request,response) {
        console.log('GET' + URL_BASE + 'users/id');
        let indice = request.params.id;//en vez de id se puede usar cualquier variable, pero debe ser la misma
        let instancia = userFile[indice - 1];
        console.log(instancia);
        let respuesta = (instancia != undefined) ? instancia : {"mensaje":"Recurso no encontrado."};
        response.status(200);
        response.send(respuesta);
        /*if(respuesta != undefined)
            response.send(instancia);
        else{
            response.send({"mensaje":"Recurso no encontrado."});
        }*/
        //console.log(request.params.id);
        //console.log(request.params);
      });

//Peticiòn GET con Query String
app.get(URL_BASE + 'usersq',
      function(request,response) {
        console.log('GET' + URL_BASE + ' con query string ');
        console.log(request.query);
        response.send(respuesta);
      });

//Peticón POST
app.post(URL_BASE + 'users',
    function(req,res){
      totalUsers = userFile.length;
      //comprobación de que existe
      cuerpo = req.body;
      let newUser = {
        userID    : totalUsers + 1,
        first_name : req.body.first_name,
        last_name  : req.body.last_name,
        email     : req.body.email,
        password  : req.body.password
      }
      userFile.push(newUser);
      res.status(200);
      res.send({"mensaje" : "Usuario creado con éxito","usuario" : newUser});
      console.log(cuerpo);
    });

//Peticón PUT
app.put(URL_BASE + 'users/:id',
    function(req,res){
      cuerpo = req.body;
      var cntBody = Object.keys(cuerpo).length;
      if (cntBody > 0){
        let idx = req.params.id - 1
        if (userFile[idx] != undefined){
          userFile[idx].first_name = cuerpo.first_name;
          userFile[idx].last_name = cuerpo.last_name;
          userFile[idx].email = cuerpo.email;
          userFile[idx].password = cuerpo.password;
          res.status(201);
          res.send({"mensaje" : "Usuario actualizado con éxito","Array" : userFile[idx]});
          console.log(userFile[idx]);
        }
        else {
          let varError = {"mensaje" : "No existe parametro para el body"}
          res.send(varError);
          console.log(varError);
        }
      }
    }
)

//Petición DELETE
app.delete(URL_BASE + 'users/:id', //en vez de "id" se puede usar cualquier variable
      function(request,response) {
        //console.log('DELETE' + URL_BASE + 'users/ids');
        //en vez de id se puede usar cualquier variable, pero debe ser la misma
        let indice = request.params.id;
        let ind = indice - 1;
        let instancia = userFile[indice - 1];
        let instancia2 = userFile[ind];
        console.log(indice);
        console.log(ind);
        console.log(instancia);
        console.log(instancia2);
        let respuesta = (instancia2 != undefined) ? instancia2 : {"mensaje":"Recurso no encontrado."};
        userFile.splice(ind,1);
        response.status(204);
        response.send(respuesta);
        /*if(respuesta != undefined)
            response.send(instancia);
        else{
            response.send({"mensaje":"Recurso no encontrado."});
        }*/
        //console.log(request.params.id);
        //console.log(request.params);
      });

//Login - User json
app.post(URL_BASE + 'login',
  function(req,res){
    var email = req.body.email;
    var pass = req.body.password;
    for (user of userFile) {
      if (user.email == email ) {
        if (user.password == pass) {
          user.logged = true;
          writeUserDataToFile(userFile);
          console.log("Login correcto");
          res.send({"Mensaje" : "Login correcto","Usuario" : user.userID})
        }
        else {
          console.log("Login incorrecto");
          res.send({"Mensaje" : "Login incorrecto"})
        }
      }
    }
  }
)

//Logout - User json
app.post(URL_BASE + 'logout/:id',
  function(req,res){
    var id = req.params.id;
    console.log(id);
    //var pass = req.body.password;
    for (user of userFile) {
      if (user.userID == id ) {
        var login = String(user.logged);
        if (login == "true") {
          delete user.logged;
          writeUserDataToFile(userFile);
          console.log("Logout correcto");
          res.send({"Mensaje" : "Logout correcto","Usuario" : user.userID})
        }
        else {
          console.log("Logout incorrecto");
          res.send({"Mensaje" : "Logout incorrecto"})
        }
      }
    }
  }
)

//Usuarios logados - JSON
app.get(URL_BASE + 'logados',
  function(req,res){
    var newUfile = [];
    for (user of userFile) {
        if (user.logged == true) {
          newUfile.push(user);
        }
    }
    res.status(200);
    res.send(newUfile)
  }
)


function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);

  fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    }
  )
}

//GET users consumiendo API REST de mLab
app.get(URL_BASE + 'mlabusers',
  function(req,res){
    console.log("GET /colapi/v3/users");
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log("Cliente HTTP mLab creado.");
    var qString = 'f={"_id":0}&';
    httpClient.get('user?' + qString + miAPIKey,
      function(err, respuestaMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo usuario"}
          res.status(500);
        }else {
          if (body.length > 0) {
            response = body;
          }else {
            response = {"msg" : "Ningún elemento 'user'"}
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)

//GET users consumiendo API REST de mLab por ID
app.get(URL_BASE + 'mlabusers/:id',
  function(req,res){
    console.log("GET /techu4db/v1/users/:id");
    var id = req.params.id;
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log("Cliente HTTP mLab creado.");
    console.log(id);
    var qString = 'q={"userID":' + id + '}&';
    var qStrField = 'f={"_id":0}&';
    console.log(qString);
    console.log(qStrField);
    httpClient.get('user?' + qString + qStrField + miAPIKey,
      function(err, respuestaMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo usuario"}
          res.status(500);
        }else {
          if (body.length > 0) {
            response = body;
          }else {
            response = {"msg" : "Ningún elemento 'user'"}
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)

//GET users consumiendo API REST de mLab por ID, usuario y cuentas
app.get(URL_BASE + 'mlabusers/:id/account',
  function(req,res){
    console.log("GET /techu4db/v1/users/:id");
    var id = req.params.id;
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log("Cliente HTTP mLab creado.");
    console.log(id);
    var qString = 'q={"userID":' + id + '}&';
    var qStrField = 'f={"_id":0}&';
    console.log(qString);
    console.log(qStrField);
    httpClient.get('account?' + qString + qStrField + miAPIKey,
      function(err, respuestaMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo usuario"}
          res.status(500);
        }else {
          if (body.length > 0) {
            response = body;
          }else {
            response = {"msg" : "Ningún elemento 'user'"}
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)

//POST users mlab
app.post(URL_BASE + 'mlabusers',
  function(req,res){
    var httpClient = requestJson.createClient(URL_MLAB);
    var qStrID = 's={"userID":-1}&l=1&';
    var qStrField = 'f={"userID":1,"_id":0}&';
    console.log(qStrID);
    console.log(qStrField);
    httpClient.get('user?' + qStrID + qStrField + miAPIKey,
      function(err, respuestaMLab, body){
        console.log(body);
        //newID = body.length + 1;
        newID = body[0].userID + 1;
        console.log("newID: " + newID);
        var newUser = {
          "userID" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password,
        };
        console.log(newUser);
        httpClient.post(URL_MLAB + "user?" + miAPIKey,newUser,
          function(err,respuesta2MLab,body){
            if (err) {
              body = {"msg" : "Error en el POST"}
              res.status(500);
            }else {
              console.log(body);
              res.status(201);
            }
            res.send(body);
          }
        )
      }
    )
  }
)

//PUT users con parámetro 'id'
app.put(URL_BASE + 'mlabusers/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"userID":' + id + '}&';
 console.log(queryStringID);
 var clienteMlab = requestJson.createClient(URL_MLAB);
 clienteMlab.get('user?'+ queryStringID + miAPIKey,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(URL_MLAB +'user?' + queryStringID + miAPIKey, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});

app.delete(URL_BASE + 'mlabusers/:id',
  function(req,res){
    var id = req.params.id;
    var qStr = 'q={"userID":' + id +'}&';
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log(id);
    console.log(qStr);
    httpClient.get('user?' + qStr + miAPIKey,
      function(err,respuestaMLab,body){
        var respuesta = body[0];
        httpClient.delete(URL_MLAB + "user/" + respuesta._id.$oid + '?'+ miAPIKey,
          function(err,respuestaMLab,body){
            res.send(body);
          }
        )
      }
  )
  }
)

app.post(URL_BASE + 'mlabusers/login',
  function(req,res){
    let email = req.body.email;
    let pass = req.body.password;
    //let queryString = 'q={"email":' + email + ',"password":' + pass + '}&';
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFlt = 'l=1&';
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log(email);
    console.log(pass);
    console.log(queryString);
    console.log(limFlt);
    httpClient.get('user?' + queryString + limFlt + miAPIKey,
      function(err,respuestaMLab,body){
        if (!err) {
          console.log(body);
          console.log(body.length);
          if (body.length == 1) {//Existe un usuario que cumple "queryString"
            let login = '{"$set" :{"logged":true}}';
            httpClient.put('user?q={"userID": ' + body[0].userID + '}&' + miAPIKey , JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function (errPut,resPut,bodyPut){
                res.send({'msg':'Login correcto','user':body[0].email, 'userID':body[0].userID});
            })
          } else {
          res.status(404).send({"msg":"Usuario no validos"});
          }
        } else {
        res.status(500).send({"msg": "Error en petición a mLab."});
        }
      }
    )
  }
)

app.post(URL_BASE + 'mlabusers/logout/:id',
  function(req,res){
    let id = req.params.id;
    let email = req.body.email;
    let pass = req.body.password;
    //let queryString = 'q={"email":' + email + ',"password":' + pass + '}&';
    let queryString = 'q={"userID":' + id +'}&';
    let limFlt = 'l=1&';
    var httpClient = requestJson.createClient(URL_MLAB);
    console.log(queryString);
    console.log(limFlt);
    httpClient.get('user?' + queryString + limFlt + miAPIKey,
      function(err,respuestaMLab,body){
        if (!err) {
          console.log(body);
          console.log(body.length);
          if (body.length == 1) {//Existe un usuario que cumple "queryString"
          
            if (body[0].logged == true) {

            let logout = '{"$unset":{"logged":true}}';
            httpClient.put('user?q={"userID": ' + body[0].userID + '}&' + miAPIKey , JSON.parse(logout),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function (errPut,resPut,bodyPut){
                res.send({'msg':'Logout correcto','user':body[0].email, 'userID':body[0].userID});
            })
          }
          } else {
          res.status(404).send({"msg":"Usuario no validos"});
          }
        } else {
        res.status(500).send({"msg": "Error en petición a mLab."});
        }
      }
    )
  }
)


app.listen(3000, function(){
  console.log('API Tech U escuchando puerto 3000...');
});
//servidor funciona y devuelve lo solicitado.

//Necesitamos un cliente.
